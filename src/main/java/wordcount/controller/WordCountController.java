package wordcount.controller;

import wordcount.domain.WordFrequency;
import wordcount.service.WordFrequencyAnalyzer;
import wordcount.service.impl.WordFrequencyAnalyzerImpl;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/word-count")
public class WordCountController {

    private WordFrequencyAnalyzer wordFrequencyAnalyzer = new WordFrequencyAnalyzerImpl();

    @Path("/ping")
    @GET
    public Response ping() {
        return Response.ok().entity("pong").build();

    }

    @Path("/calc-highest-frequency/{text}")
    @GET
    public Response calculcateHighestFrequency(@PathParam("text") String text) {
        int frequency = wordFrequencyAnalyzer.calculcateHighestFrequency(text);

        return Response.ok().entity(frequency).build();

    }

    @Path("/calc-frequency-for-word/{text}/{word}")
    @GET
    public Response calculateFrequencyForWord(@PathParam("text") String text, @PathParam("word") String word) {
        int frequency = wordFrequencyAnalyzer.calculateFrequencyForWord(text, word);

        return Response.ok().entity(frequency).build();
    }

    @Path("/calc-most-frequent-words/{text}/{count}")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response calculateMostFrequentWords(@PathParam("text") String text, @PathParam("count") int count) {
        List<WordFrequency> wordFrequencies = wordFrequencyAnalyzer.calculateMostFrequentWords(text, count);

        GenericEntity<List> list = new GenericEntity<List> (wordFrequencies) {};
        return Response.ok().entity(list).build();
    }
}

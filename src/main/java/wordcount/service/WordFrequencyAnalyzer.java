package wordcount.service;

import wordcount.domain.WordFrequency;

import java.util.List;

public interface WordFrequencyAnalyzer {
    int calculcateHighestFrequency(String text);
    int calculateFrequencyForWord(String text, String word);
    List<WordFrequency> calculateMostFrequentWords(String text, int n);
}

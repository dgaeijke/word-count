package wordcount.service.impl;

import wordcount.domain.WordFrequency;
import wordcount.domain.impl.WordFrequencyImpl;
import wordcount.service.WordFrequencyAnalyzer;
import wordcount.util.ChainComparator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WordFrequencyAnalyzerImpl implements WordFrequencyAnalyzer {

    public int calculcateHighestFrequency(String text) {
        if(text == null || text.isEmpty()) {
            return 0;
        }

        String[] words = text.split(" ");

        List<WordFrequency> wordFrequencies = calculateFrequencies(words);

        return getHighestFrequency(wordFrequencies);
    }

    public int calculateFrequencyForWord(String text, String word) {
        if(text == null || word == null || text.isEmpty() || word.isEmpty()) {
            return 0;
        }

        String[] words = text.split(" ");
        return (int) Arrays.stream(words).filter(w -> w.toLowerCase().equals(word.toLowerCase())).count();
    }

    public List<WordFrequency> calculateMostFrequentWords(String text, int n) {
        if(text == null || text.isEmpty()) {
            return new ArrayList<>();
        }

        String[] words = text   .split(" ");
        List<WordFrequency> wordFrequencies = calculateFrequencies(words);

        if(n > wordFrequencies.size()) {
            return wordFrequencies;
        } else {
            return wordFrequencies.subList(0, n);
        }
    }

    private List<WordFrequency> calculateFrequencies(String[] words) {
        ArrayList<WordFrequency> wordList = new ArrayList<>();

        Arrays.stream(words).forEach(word -> {
            WordFrequency wordFrequency = wordList.stream().filter(w -> w.getWord().equals(word.toLowerCase())).findFirst().orElse(null);
            if(wordFrequency != null) {
                wordFrequency.setFrequency(wordFrequency.getFrequency() +1);
            } else {
                wordList.add(new WordFrequencyImpl(word.toLowerCase()));
            }
        });

        wordList.sort(new ChainComparator());

        return wordList;
    }

    private int getHighestFrequency(List<WordFrequency> wordList) {
        if(wordList.size() > 0) {
            return wordList.get(0).getFrequency();
        }
        return 0;
    }
}
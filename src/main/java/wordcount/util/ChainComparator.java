package wordcount.util;

import wordcount.domain.WordFrequency;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ChainComparator implements Comparator<WordFrequency> {
    private List<Comparator<WordFrequency>> comparatorList;

    public ChainComparator() {
        comparatorList = new ArrayList<>();
        Comparator<WordFrequency> sortByFrequency = Comparator.comparing(WordFrequency::getFrequency).reversed();
        Comparator<WordFrequency> sortByWord = Comparator.comparing(WordFrequency::getWord);

        comparatorList.add(sortByFrequency);
        comparatorList.add(sortByWord);
    }

    @Override
    public int compare(WordFrequency w1, WordFrequency w2) {
        int result;
        for(Comparator<WordFrequency> comparator : comparatorList) {
            if ((result = comparator.compare(w1, w2)) != 0) {
                return result;
            }
        }
        return 0;
    }
}

package wordcount.domain.impl;

import wordcount.domain.WordFrequency;

public class WordFrequencyImpl implements WordFrequency {

    private String word;
    private int frequency;

    public WordFrequencyImpl(String word) {
        this.word = word;
        this.frequency = 1;
    }

    public String getWord() {
        return word;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }
}

package wordcount.domain;

public interface WordFrequency {
    String getWord();
    int getFrequency();
    void setFrequency(int frequency);
}

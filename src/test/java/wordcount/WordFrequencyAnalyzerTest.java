package wordcount;

import org.junit.Test;
import wordcount.domain.WordFrequency;
import wordcount.service.WordFrequencyAnalyzer;
import wordcount.service.impl.WordFrequencyAnalyzerImpl;

import java.util.List;

public class WordFrequencyAnalyzerTest {

    private WordFrequencyAnalyzer wordFrequencyAnalyzer = new WordFrequencyAnalyzerImpl();
    private static final String TEXT_SHORT = "The sun shines over the lake";
    private static final String TEXT_LONG = "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen.";

    @Test
    public void calculateHighestFrequencyShortTextSuccessTest() {
        int frequency = wordFrequencyAnalyzer.calculcateHighestFrequency(TEXT_SHORT);

        assert(frequency == 2);
    }

    @Test
    public void calculateHighestFrequencyLongTextSuccessTest() {
        int frequency = wordFrequencyAnalyzer.calculcateHighestFrequency(TEXT_LONG);

        assert(frequency == 12);
    }

    @Test
    public void calculateHighestFrequencyEmptyTest() {
        int frequency = wordFrequencyAnalyzer.calculcateHighestFrequency("");

        assert(frequency == 0);
    }

    @Test
    public void calculateHighestFrequencyNullTest() {
        int frequency = wordFrequencyAnalyzer.calculcateHighestFrequency(null);

        assert(frequency == 0);
    }

    @Test
    public void calculateFrequencyForWordSuccessTest() {
        int frequency1 = wordFrequencyAnalyzer.calculateFrequencyForWord(TEXT_LONG, "Far");
        int frequency2 = wordFrequencyAnalyzer.calculateFrequencyForWord(TEXT_LONG, "the");

        assert(frequency1 == 4);
        assert(frequency2 == 12);
    }

    @Test
    public void calculateFrequencyForWordEmptyTextTest() {
        int frequency = wordFrequencyAnalyzer.calculateFrequencyForWord(TEXT_LONG, "");

        assert(frequency == 0);
    }

    @Test
    public void calculateFrequencyForWordNullTextTest() {
        int frequency = wordFrequencyAnalyzer.calculateFrequencyForWord(TEXT_LONG, null);

        assert(frequency == 0);
    }

    @Test
    public void calculateFrequencyForWordEmptyWordTest() {
        int frequency = wordFrequencyAnalyzer.calculateFrequencyForWord("", "test");

        assert(frequency == 0);
    }

    @Test
    public void calculateFrequencyForWordNullWordTest() {
        int frequency = wordFrequencyAnalyzer.calculateFrequencyForWord(null, "test");

        assert(frequency == 0);
    }

    @Test
    public void calculateMostFrequentWordsShortTextSuccessTest() {
        List<WordFrequency> wordFrequencies = wordFrequencyAnalyzer.calculateMostFrequentWords(TEXT_SHORT, 3);

        assert(wordFrequencies != null);
        assert(wordFrequencies.size() == 3);

        WordFrequency wordFrequency = wordFrequencies.get(0);
        assert(wordFrequency.getWord().equals("the"));
        assert(wordFrequency.getFrequency() == 2);

        wordFrequency = wordFrequencies.get(1);
        assert(wordFrequency.getWord().equals("lake"));
        assert(wordFrequency.getFrequency() == 1);

        wordFrequency = wordFrequencies.get(2);
        assert(wordFrequency.getWord().equals("over"));
        assert(wordFrequency.getFrequency() == 1);
    }

    @Test
    public void calculateMostFrequentWordsShortTextPartialSuccessTest() {
        List<WordFrequency> wordFrequencies = wordFrequencyAnalyzer.calculateMostFrequentWords(TEXT_SHORT, 10);

        assert(wordFrequencies != null);
        assert(wordFrequencies.size() == 5);

        WordFrequency wordFrequency = wordFrequencies.get(0);
        assert(wordFrequency.getWord().equals("the"));
        assert(wordFrequency.getFrequency() == 2);

        wordFrequency = wordFrequencies.get(1);
        assert(wordFrequency.getWord().equals("lake"));
        assert(wordFrequency.getFrequency() == 1);

        wordFrequency = wordFrequencies.get(2);
        assert(wordFrequency.getWord().equals("over"));
        assert(wordFrequency.getFrequency() == 1);

        wordFrequency = wordFrequencies.get(3);
        assert(wordFrequency.getWord().equals("shines"));
        assert(wordFrequency.getFrequency() == 1);

        wordFrequency = wordFrequencies.get(4);
        assert(wordFrequency.getWord().equals("sun"));
        assert(wordFrequency.getFrequency() == 1);
    }

    @Test
    public void calculateMostFrequentWordsEmptyTest() {
        List<WordFrequency> wordFrequencies = wordFrequencyAnalyzer.calculateMostFrequentWords("", 3);

        assert(wordFrequencies != null);
        assert(wordFrequencies.size() == 0);
    }

    @Test
    public void calculateMostFrequentWordsNullTest() {
        List<WordFrequency> wordFrequencies = wordFrequencyAnalyzer.calculateMostFrequentWords(null, 3);

        assert(wordFrequencies != null);
        assert(wordFrequencies.size() == 0);
    }
}


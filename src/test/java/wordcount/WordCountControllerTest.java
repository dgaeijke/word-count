package wordcount;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import wordcount.controller.WordCountController;
import wordcount.domain.WordFrequency;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

public class WordCountControllerTest extends JerseyTest {

    private static final String TEXT_SHORT = "The sun shines over the lake";

    @Override
    protected Application configure() {
        return new ResourceConfig(WordCountController.class);
    }

    @Test
    public void pingTest() {
        String response = target("/word-count/ping").request().get(String.class);

        assert(response.equals("pong"));
    }

    @Test
    public void calculateHighestFrequencySuccessTest() {
        String response = target("/word-count/calc-highest-frequency/" + TEXT_SHORT).request().get(String.class);

        assert(response.equals("2"));
    }

    @Test
    public void calculateFrequencyForWordSuccessTest() {
        String word = "sun";
        String response = target("/word-count/calc-frequency-for-word/" + TEXT_SHORT + "/" + word).request().get(String.class);

        assert(response.equals("1"));
    }

    @Test
    public void calculateMostFrequentForWordsSuccessTest() {
        Response response = target("/word-count/calc-most-frequent-words/" + TEXT_SHORT + "/5").request().get(Response.class);
        List<WordFrequency> wordFrequencies = response.readEntity(new GenericType<List<WordFrequency>>() {});

        assert(wordFrequencies != null);
        assert(wordFrequencies.size() == 5);

        WordFrequency wordFrequency = wordFrequencies.get(0);
        assert(wordFrequency.getWord().equals("the"));
        assert(wordFrequency.getFrequency() == 2);

        wordFrequency = wordFrequencies.get(1);
        assert(wordFrequency.getWord().equals("lake"));
        assert(wordFrequency.getFrequency() == 1);

        wordFrequency = wordFrequencies.get(2);
        assert(wordFrequency.getWord().equals("over"));
        assert(wordFrequency.getFrequency() == 1);

        wordFrequency = wordFrequencies.get(3);
        assert(wordFrequency.getWord().equals("shines"));
        assert(wordFrequency.getFrequency() == 1);

        wordFrequency = wordFrequencies.get(4);
        assert(wordFrequency.getWord().equals("sun"));
        assert(wordFrequency.getFrequency() == 1);
    }
}
